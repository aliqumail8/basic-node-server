const express = require("express");
const ejs = require("ejs");
const bodyParser = require("body-parser");

let list = [
  {
    id: 1,
    name: "Pakistan",
    capital: "Islamabad",
  },
  {
    id: 2,
    name: "India",
    capital: "Delhi",
  },
  {
    id: 3,
    name: "England",
    capital: "London",
  },
];

const app = express();

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.render("main", { list: list });
});

app.post("/post", (req, res) => {
  console.log(req.body);
  list.push({
    id: req.body.id,
    name: req.body.name,
    capital: req.body.capital,
  });
  res.redirect("/");
});


app.put("/put/:id", (req, res) => {
    const {id} = req.params
    for (let i =0; i<list.length; i++){
       if (list[i].id == id){
        if (req.query.name && req.query.capital){
          list[i].name = req.query.name;
          list[i].capital = req.query.capital;
          break;   
        } 
       
       } 
    }
    res.redirect("/");
});

app.delete("/delete/:id", (req,res)=>{
    const {id} = req.params;
    list = list.filter(item => item.id != id);
    console.log("successfully deleted");
    res.redirect("/")
});



app.patch("/patch/:id", (req, res) => {
  const {id} = req.params
  for (let i =0; i<list.length; i++){
     if (list[i].id == id){
      if (req.query.name) list[i].name = req.query.name;
      if(req.query.capital) list[i].capital = req.query.capital;
      break;
     } 
  }
  res.redirect("/");
});

app.listen(3000, () => {
  console.log("server started at port:3000");
});
